<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>H+ 后台主题UI框架 - 基础表格</title>

    +6665<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">


<jsp:include page="/layout/css.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/plugins/bootstrap-table/bootstrap-table.min.css">

<link href="${pageContext.request.contextPath}/css/plugins/iCheck/custom.css" rel="stylesheet">


</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>基本</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="table_basic.html#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="table_basic.html#">选项1</a>
                                </li>
                                <li><a href="table_basic.html#">选项2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table id="table"></table>
                        <div class="btn btn-primary" id="removeAll">删除所选</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="/layout/script.jsp"/>
    <script src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/bootstrap-table-zh-CN.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/bootstrap-table-filter-control.js"></script>

    <!-- Peity -->
    <script src="${pageContext.request.contextPath}/js/plugins/peity/jquery.peity.min.js"></script>
    <!-- 自定义js -->
    <script src="${pageContext.request.contextPath}/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="${pageContext.request.contextPath}/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="${pageContext.request.contextPath}/js/demo/peity-demo.js"></script>
    <script>
        $('#table').bootstrapTable({
            url:'${pageContext.request.contextPath}/StudentServlet/listAllByPaginationSearch',
            pagination:true,
            pageSize:5,
            pageList:[5,10,'All'],
            search:true,
            showColumns:true,
            showRefresh:true,
            // clickToSelect:true,
            sidePagination:'server',
            filterControl:true,
            columns: [{
                checkbox:true
            },{
                field: 'name',
                title: '姓名',
                filterControl:'input',
            }, {
                field: 'age',
                title: '年龄',
                formatter:'ageFormatter',
                filterControl:'input',
            }, {
                field: 'gender',
                title: '性别',
                formatter:'genderFormatter',
                filterControl:'select',
            },{
                title:'操作',
                formatter:'czFormatter',
                events:'operateEvents'
            }],
            rowStyle:function(row,index){
                if (index % 2 == 0) {
                    return {
                        classes: 'success'
                    };
                }else{
                    return {
                        classes: 'default'
                    };
                }
                return {};
            }
        });
        function ageFormatter(v1){
            return v1+'岁';
        }
        function genderFormatter(v1){
            if(v1==0){
                return '男';
            }else if(v1==1){
                return '女';
            }
        }
        function czFormatter(row,index){
            return [
                '<div>',
                '<a class="edit" href="javascript:void(0)" title="edit">',
                '<i class="glyphicon glyphicon-pencil"></i>',
                '</a>&nbsp;&nbsp;&nbsp;&nbsp;',
                '<a class="remove" href="javascript:void(0)" title="remove">',
                '<i class="glyphicon glyphicon-remove"></i>',
                '</a>',
                '</div>'
            ].join('');
        }
        window.operateEvents={
            //'click .edit' 中间有空格
            'click .edit':function(e,value,row){
                $('#test [name=name]').val(row['name']);
                $('#test [name=age]').val(row['age']);
                //测试时丢了传id
                $('#test [name=id]').val(row['id']);
                if(row['gender']==0){
                    $('#test[name=gender][value=0]').prop('checked',true);
                }else if(row['gender']==1){
                    $('#test[name=gender][value=1]').prop('checked',true);
                }
                inde = layer.open({
                    type:1,
                    content:$('#test'),
                    area:['500px','405px']
                });
            },
            'click .remove':function(e,value,row){
                if(confirm('确定删除当前选中？')){
                    //ajax删除对应id
                    $.post('${pageContext.request.contextPath}/StudentServlet/deleteById?id='+row['id'],function (r) {
                        if(r){
                            $('#table').bootstrapTable('refresh');
                        }else{
                            alert("失败");
                        }
                    });
                }
            }
        };
        $('#removeAll').click(function(){
                if(confirm("确定删除所选?")){
                    json_arr = $('#table').bootstrapTable('getSelections');
                    str ="";
                    for(i in json_arr){
                        str+=json_arr[i]['id']+",";
                    }
                    str2 = str.substring(0,str.length-1);
                    $.post('${pageContext.request.contextPath}/StudentServlet/removeAll?ids='+str2,function(r){
                        if(r){
                            $('#table').bootstrapTable('refresh');
                        }else{
                            alert("失败");
                        }    });

                }
        });
        //放在$(function( 内部 ))  相当于加载全页面   window.onload
        $(function () {
            $('#sub').click(function () {
                $.post('${pageContext.request.contextPath}/StudentServlet/updateById',$('form').serialize(),function (r) {
                    if(r){
                        layer.msg('修改成功');
                        //关闭弹层
                        layer.close(inde);
                        //刷新表格
                        $('#table').bootstrapTable('refresh');
                    }else{
                        layer.msg('修改失败');
                    }
                });
            });
        });

    </script>

</body>
<div class="row" id="test" style="display: none">
    <div class="col-sm-12" >
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>简单示例</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="form_basic.html#">选项1</a>
                        </li>
                        <li><a href="form_basic.html#">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal m-t">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">姓名：</label>
                        <div class="col-sm-8">
                            <input id="cname" name="name" minlength="2" type="text" class="form-control" required="" aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">年龄：</label>
                        <div class="col-sm-8">
                            <input  class="form-control" name="age" required="" aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">性别：</label>
                        <div class="col-sm-8">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="0" checked="">
                                    男
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="1">
                                    女
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-3">
                            <div class="btn btn-primary" id="sub">提交</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</html>