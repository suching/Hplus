<%--
  Created by IntelliJ IDEA.
  User: 20161003
  Date: 2018/10/14
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="utf-8"%>

    <link href="${pageContext.request.contextPath}/img/favicon.ico" rel="shortcut icon">
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css?v=4.1.0" rel="stylesheet">

