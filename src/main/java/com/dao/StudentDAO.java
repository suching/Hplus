package com.dao;
import com.bean.Student;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface StudentDAO {
   /*
   * 查询所有内容
   **/
    List<Student> selectAll() throws SQLException;
    /*
    * 根据id查询内容
    */
    Student selectById(Integer id) throws SQLException;
    /*
     * 插入
     */
    boolean insert(Student stu) throws SQLException;
    /*
     * 删除
     */
    boolean deleteById(Integer id) throws SQLException;
    /*
     * 修改
     */
    boolean update(Student stu) throws SQLException;
    /*
     * 总记录条数
     */
    Integer count() throws SQLException;

    Integer countSearch(Map map) throws SQLException;

    boolean deleteAll(String[] idsArr) throws  SQLException;

    List<Student> selectAllPagination(Integer offset,Integer limit) throws SQLException;

    List<Student> selectAllPaginationSearch(Integer offset, Integer limit, Map map) throws SQLException;

   }
