package com.service.impl;

import com.bean.Student;
import com.dao.StudentDAO;
import com.dao.impl.StudentDAOImpl;
import com.service.StudentService;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class StudentServiceImpl implements StudentService {

    StudentDAO studentDAO = new StudentDAOImpl();

    @Override
    public List<Student> selectAll() throws SQLException {
        return studentDAO.selectAll();
    }

    @Override
    public boolean deleteById(Integer id) throws SQLException {
        return studentDAO.deleteById(id);
    }

    @Override
    public boolean removeAll(String[] idsArr) throws SQLException {
        return studentDAO.deleteAll(idsArr);
    }

    @Override
    public List<Student> selectAllPagination(Integer offset, Integer limit) throws SQLException {
        return studentDAO.selectAllPagination(offset,limit);
    }

    @Override
    public boolean insert(Student s) throws SQLException {
        return studentDAO.insert(s);
    }

    @Override
    public Integer count() throws SQLException {
        return studentDAO.count();
    }

    @Override
    public Integer countSearch(Map map) throws SQLException {
        return studentDAO.countSearch(map);
    }

    @Override
    public boolean update(Student s) throws SQLException {
        return studentDAO.update(s);
    }

    @Override
    public List<Student> selectAllPaginationSearch(Integer offset, Integer limit, Map map) throws SQLException {
        return studentDAO.selectAllPaginationSearch(offset,limit,map);
    }
}
