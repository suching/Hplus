package com.service;

import com.bean.Student;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface StudentService {

    List<Student> selectAll() throws SQLException;

    boolean deleteById(Integer id) throws  SQLException;

    boolean  removeAll(String[] idsArr)throws  SQLException;

    List<Student> selectAllPagination(Integer offset,Integer limit) throws SQLException;

    boolean insert(Student s)throws  SQLException;

    Integer count() throws  SQLException;

    Integer countSearch(Map map) throws SQLException;

    boolean update(Student s) throws  SQLException;

    List<Student> selectAllPaginationSearch(Integer offset, Integer limit,Map map) throws SQLException;
}
