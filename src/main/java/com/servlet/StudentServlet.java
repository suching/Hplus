package com.servlet;

import com.bean.Student;
import com.google.gson.Gson;
import com.service.StudentService;
import com.service.impl.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/StudentServlet/*")
public class StudentServlet extends HttpServlet {

    StudentService studentService = new StudentServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html,charset=utf-8");   //命令浏览器用什么编码打开文件

        String pathInfo = request.getPathInfo().substring(1);
        if("listAll".equals(pathInfo)){
            listAll(request,response);
        }else if("deleteById".equals(pathInfo)){
            deleteById(request,response);
        }else if("removeAll".equals(pathInfo)){
            removeAll(request,response);
        }else if("listAllByPagination".equals(pathInfo)){
            listAllByPagination(request,response);
        }else if("add".equals(pathInfo)){
            add(request,response);
        }else if("updateById".equals(pathInfo)){
            updateById(request,response);
        }else if("listAllByPaginationSearch".equals(pathInfo)){
            listAllByPaginationSearch(request,response);
        }

    }

    private void listAllByPaginationSearch(HttpServletRequest request, HttpServletResponse response) {
            int offset = Integer.valueOf(request.getParameter("offset"));
            int limit = Integer.valueOf(request.getParameter("limit"));
            //String search = request.getParameter("search");
            String filter  = request.getParameter("filter");
        System.out.println("---"+filter);
            Gson gson = new Gson();
            Map<String,String> map1 = gson.fromJson(filter,Map.class);
            System.out.println("+++"+map1);

        try {
            List<Student> studentList = studentService.selectAllPaginationSearch(offset,limit,map1);
            int total = studentService.countSearch(map1);
            Map<String,Object> map = new HashMap<>();
            map.put("total",total);
            map.put("rows",studentList);

            String s = gson.toJson(map);
            response.getWriter().print(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateById(HttpServletRequest request, HttpServletResponse response) {
        Integer id = Integer.valueOf(request.getParameter("id"));
        String name = request.getParameter("name");
        Integer age = Integer.valueOf(request.getParameter("age"));
        String gender = request.getParameter("gender");
        Student s = new Student(id,name,age,gender);
        try {
            boolean b = studentService.update(s);
            if(b){
                response.getWriter().print("success");
            }else{
                response.getWriter().print("");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void add(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        Integer age = Integer.valueOf(request.getParameter("age"));
        String gender = request.getParameter("gender");

        Student s = new Student(null,name,age,gender);
        System.out.println(s.getName()+s.getAge()+s.getGender());
        try {
            boolean b = studentService.insert(s);
            if(b){
                request.setAttribute("msg","新增成功");
            }else{
                request.setAttribute("msg","新增失败");
            }
            request.getRequestDispatcher("/student/student_add.jsp").forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void listAllByPagination(HttpServletRequest request, HttpServletResponse response) {
        int offset = Integer.valueOf(request.getParameter("offset"));
        int limit = Integer.valueOf(request.getParameter("limit"));
        try {
            List<Student> studentList = studentService.selectAllPagination(offset,limit);
            int total = studentService.count();
            Map<String,Object> map = new HashMap<>();
            map.put("total",total);
            map.put("rows",studentList);

            Gson gson = new Gson();
            String s =gson.toJson(map);
            response.getWriter().print(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeAll(HttpServletRequest request, HttpServletResponse response) {

        String ids = request.getParameter("ids");
        String[] idsArr = ids.split(",");
        try {
            boolean b = studentService.removeAll(idsArr);
            if(b){
                response.getWriter().print("删除成功");
            }else{
                response.getWriter().print("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteById(HttpServletRequest request, HttpServletResponse response) {

        int id = Integer.valueOf(request.getParameter("id"));
        try {
            boolean b = studentService.deleteById(id);
            if(b){
                response.getWriter().print("删除成功");
            }else{
                response.getWriter().print("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void listAll(HttpServletRequest request, HttpServletResponse response) {

        try {
            List<Student> studentList = studentService.selectAll();
//            request.setAttribute("studentList",studentList);
//            request.getRequestDispatcher("/student/student_list.jsp").forward(request,response);
            System.out.println(studentList);
            //将内容主动传到页面
            Gson gson = new Gson();
            String s =gson.toJson(studentList);
            response.getWriter().print(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
