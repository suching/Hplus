package com.test;

import com.bean.Student;
import com.dao.StudentDAO;
import com.dao.impl.StudentDAOImpl;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

    public class StudentTest {

        StudentDAO studentDAO = new StudentDAOImpl();

        @Test
    public void test1() throws SQLException {
        List<Student> studentList = studentDAO.selectAll();
        System.out.println(studentList);
    }

    @Test
    public void test2()throws SQLException{
        Student student = studentDAO.selectById(1);
        System.out.println(student);
    }
    @Test
    public void test3() throws SQLException{
        Student s = new Student();
        s.setName("傻子");
        s.setAge(1);
        s.setGender("111");
        studentDAO.insert(s);
    }
    @Test
    public void test4() throws SQLException{
        boolean b = studentDAO.deleteById(5);
        System.out.println(b);
    }
    @Test
    public void test5() throws SQLException{
        Student s = studentDAO.selectById(4);
        s.setAge(35);
        boolean b = studentDAO.update(s);
        System.out.println(b);
    }
    @Test
    public void test6() throws SQLException{
        Integer count = studentDAO.count();
        System.out.println(count);
    }
}
